
((selectedTheme)=> {	modeSwitcher(selectedTheme)})('light');

function modeSwitcher(selectedTheme) {

	const currentTheme = sessionStorage.getItem('theme');

	const themeMap = {
		'dark': 'light',
		'light': 'dark',
	};

	const targetTheme = selectedTheme ?? themeMap[currentTheme];

	document.documentElement.setAttribute('data-theme', targetTheme);
	sessionStorage.setItem('theme', targetTheme);

}
