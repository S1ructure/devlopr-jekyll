---
layout: post
title:  "Welcome"
author: Florian
date: '2022-05-01 21:11:00 +0200'
keywords: florian hofsaess blog welcome
permalink: /blog/2022/05/welcome/
---

## Herzlich willkommen!

Schön, dass du hier bist und direkt angefangen hast in den Blog Posts zu stöbern. 
Ich habe lange überlegt, ob es eine deutsche oder eine englische Ausführung werden soll - ich habe mich - erstmal - für eine deutsche Variante entschieden, da das Thema des Bloggens auch für mich noch neu ist ;)

### Inhalt des Blogs

Ich versuche hier Themen, denen ich im Rahmen meiner Tätigkeit als TeamLead und Softwareentwickler begegne und als spannend erachte, hier zu präsentieren.

Diese Themen werden folglich meist in irgendeiner Form mit IT zu tun haben. Gemessen an meiner bisherigen Erfahrung werden das viele Themen rund um Softwareentwicklung und Smart-Home sein.

Seltener, aber durchaus denkbar, wird es konkrete Tutorials geben. Ich lasse mich hier leiten von dem was ich für spannend halte und von etwaigem Feedback, das ich erhalten werde.

### Warum?

Ja - warum einen Blog und warum kein Buch.

Eigentlich hatte ich eine wunderbar funktionierende Webseite die ich auf Basis von dem [CMS Sulu](https://sulu.io/) aufgebaut habe und die bisher auch ohne Blog gut ausgekommen ist.
Ich wollte mich jedoch im Bereich AWS weiterbilde und merke, dass ich mir neue Themen deutlich intensiver anschaue, wenn ich dazu auch praxibezogene Projekte habe.

Also habe ich meine Website auf Basis von [Jerkyll](https://jekyllrb.com/) neu gebaut und möchte diese hier via AWS hosten



